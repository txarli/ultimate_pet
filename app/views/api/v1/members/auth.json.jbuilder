json.status @response[:status]
json.message @response[:message] if @response[:message]
json.token @response[:token] if @response[:token]
json.member_id @response[:member_id] if @response[:member_id]
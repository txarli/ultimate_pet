json.array!(@pets) do |pet|
  json.extract! pet, :id, :name, :age, :sex, :kind, :wins, :member_id
  json.member pet.member.name
end

ActiveAdmin.register Member do
  permit_params :email, :password, :password_confirmation, :position, :name

  index do
    selectable_column
    id_column
    column :email
    column :current_sign_in_at
    column :sign_in_count
    column :created_at
    actions
  end

  filter :email
  filter :current_sign_in_at
  filter :sign_in_count
  filter :created_at

  form do |f|
    f.inputs "Admin Details" do
      f.input :position, as: :select, collection: Member::ROLES, prompt: "Select position"
      f.input :name
      f.input :email
      f.input :password
      f.input :password_confirmation
    end
    f.actions
  end


  show do
    panel "Member data" do
      table_for member do
        column :name
        column :email
        column :position
      end
    end
    active_admin_comments
  end

  sidebar "Details", only: :show do
    attributes_table_for member do
      row :id
      row :authentication_token
      row :sign_in_count
      row :last_sign_in_at
      row :last_sign_in_ip
      row :created_at
    end
  end

end

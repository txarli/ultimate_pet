module Api
  module V1
    class MembersController < ApplicationController

      before_action :set_member, only: [:auth]

      respond_to :json
      # Service for remote user authentication
      #
      # == Parameters:
      # email::
      #   unique mail identifier
      # password::
      #   plaintext password
      #
      # == Returns:
      #   json response according to validations and statements result.
      #
      def auth
        if @member && @member.valid_password?(params[:password])
          @response = {status: 'ok', token: @member.authentication_token, member_id: @member.id}
        else
          @response = {status: 'ko', message: "User not found or password do not match"}
        end
      end

      private
      # Use callbacks to share common setup or constraints between actions.
      def set_member
        @member = Member.find_by(email: params[:email])
      end

    end
  end
end
module Api
  module V1
    class PetsController < ApplicationController

      acts_as_token_authentication_handler_for Member

      before_action :set_pet, only: [:show, :update, :destroy]
      before_action :set_member, only: [:create, :show, :update, :destroy]

      def index
        @pets = Pet.all
      end

      def show
      end

      def create
        @pet = Pet.new(pet_params)
        @pet.member = @member
        if @pet.save
          render :show, status: :created, location: @pet
        else
          render json: @pet.errors, status: :unprocessable_entity
        end
      end

      def update
        if pet_owner?
          if @pet.update(pet_params)
            render :show, status: :ok, location: @pet
          else
            render json: @pet.errors, status: :unprocessable_entity
          end
        end
      end

      def destroy
        if pet_owner?
          @pet.destroy
        end
      end

      private

      def pet_owner?
        @member.id == @pet.member_id ? true : false
      end

      def set_member
        @member = Member.find_by(authentication_token: params[:token])
      end

      def set_pet
        @pet = Pet.find(params[:id])
      end

      def pet_params
        params.require(:pet).permit(:name, :age, :sex, :kind, :wins, :member_id)
      end

    end
  end
end
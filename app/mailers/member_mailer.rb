class MemberMailer < ApplicationMailer

  def welcome_email(member)
    @member = member
    @url  = 'http://ultimate-pet.dev'
    mail(to: @member.email, subject: 'Welcome to groopify ultimate pet!')
  end

end

# == Schema Information
#
# Table name: pets
#
#  id         :integer          not null, primary key
#  name       :string
#  age        :string
#  sex        :string
#  kind       :string
#  wins       :string
#  member_id  :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Pet < ActiveRecord::Base

  validates_presence_of :name, :age, :sex, :kind

  belongs_to :member

end

# == Schema Information
#
# Table name: members
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string
#  last_sign_in_ip        :string
#  created_at             :datetime
#  updated_at             :datetime
#  name                   :string
#  position               :string
#  authentication_token   :string
#

class Member < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, 
         :recoverable, :rememberable, :trackable, :validatable

  acts_as_token_authenticatable

  validates_presence_of :name, :position

  after_create :send_welcome_mail

  has_many :pets

  ROLES = ["CEO",
           "CIO",
           "CTO",
           "Backend Dev RoR",
           "Data Scientist",
           "Design",
           "Inside Sales",
           "Junior Dev RoR",
           "MK & Biz",
           "Mobile Mk Expert",
           "Online Marketing",
           "Ops & HR Manager",
           "Senior Android Developer",
           "Senior iOS Developer",
           "Software Architect",
           "So awesome that needs a new category!"]

  private

  def send_welcome_mail
    MemberMailer.welcome_email(self).deliver_now
  end

end

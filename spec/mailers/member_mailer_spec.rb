require "rails_helper"

RSpec.describe MemberMailer, type: :mailer do
  describe 'instructions' do

    let(:member) { FactoryGirl.create(:member) }
    let(:mail) { MemberMailer.welcome_email(member) }

    it 'renders the subject' do
      expect(mail.subject).to eql('Welcome to groopify ultimate pet!')
    end

    it 'renders the receiver email' do
      expect(mail.to).to eql([member.email])
    end

    it 'renders the sender email' do
      expect(mail.from).to eql(['from@example.com'])
    end

    it 'assigns @name' do
      expect(mail.body.encoded).to match(member.name)
    end

    it 'shows backend url' do
      expect(mail.body.encoded).to match("http://ultimate-pet.dev")
    end
  end

end

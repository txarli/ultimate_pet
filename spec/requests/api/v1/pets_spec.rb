require 'spec_helper'

describe "Pets" do

  describe "GET /api/v1/pets" do

    it 'should return 401 unless user token is present and right' do
      get api_v1_pets_path, {format: :json}
      expect(response.status).to eq(401)
    end

    it 'should return 401 unless user token is present and right' do
      member = FactoryGirl.create(:member)
      get api_v1_pets_path(member_email: member.email, member_token: member.authentication_token), {format: :json}
      expect(response.status).to eq(200)
    end

  end

  #TODO: Rest of specs are pending to write

end
require 'spec_helper'

describe "Members" do

  describe "GET /api/v1/members/auth" do

    let!(:member) { FactoryGirl.create(:member) }

    it "should be available and respond with a ko if no auth data present" do
      get auth_api_v1_members_path, {format: :json}
      expect(response.status).to eq(200)
      expect(response.body).to match(/User not found or password do not match/)
    end

    it 'should return ok if member data checks out' do
      get auth_api_v1_members_path(email: member.email, password: member.password), {format: :json}
      expect(response.status).to eq(200)
      expect(response.body).to match(/\"status\":\"ok\"/)
    end

    it 'should return member token if data checks out' do
      get auth_api_v1_members_path(email: member.email, password: member.password), {format: :json}
      expect(response.status).to eq(200)
      expect(response.body).to match(/\"token\":/)
    end

  end

end
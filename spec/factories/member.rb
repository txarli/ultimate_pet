FactoryGirl.define do

  factory :member do
    name "JohnDoe"
    sequence(:email) { |n| "foo#{n}@example.com" }
    password "foobar099"
    position "CEO"
  end


end
FactoryGirl.define do

  factory :pet do

    name "Chewie"
    age 234
    sex "male"
    kind "Wookie"
    wins 122

  end


end
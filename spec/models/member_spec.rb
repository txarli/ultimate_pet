# == Schema Information
#
# Table name: members
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string
#  last_sign_in_ip        :string
#  created_at             :datetime
#  updated_at             :datetime
#  name                   :string
#  position               :string
#  authentication_token   :string
#

require 'rails_helper'

RSpec.describe Member, type: :model do

  it "has a valid factory" do
    member = attributes_for :member
    expect(member.class).to eq(Hash)
  end

  describe "#new" do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:email) }
    it { should validate_presence_of(:position) }
  end

   it { should have_many(:pets) }

end

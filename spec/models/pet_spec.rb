# == Schema Information
#
# Table name: pets
#
#  id         :integer          not null, primary key
#  name       :string
#  age        :string
#  sex        :string
#  kind       :string
#  wins       :string
#  member_id  :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe Pet, type: :model do

  it "has a valid factory" do
    pet = attributes_for :pet
    expect(pet.class).to eq(Hash)
  end

  describe "#new" do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:age) }
    it { should validate_presence_of(:sex) }
    it { should validate_presence_of(:kind) }
  end

  it { should belong_to :member }

end
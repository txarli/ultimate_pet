class CreatePets < ActiveRecord::Migration
  def change
    create_table :pets do |t|
      t.string :name
      t.string :age
      t.string :sex
      t.string :kind
      t.string :wins
      t.references :member, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end

class AddFieldsToMember < ActiveRecord::Migration
  def change
    add_column :members, :name, :string
    add_column :members, :position, :string
  end
end
